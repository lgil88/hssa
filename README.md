# HSSA

This is a work-in-progress implementation of the *Hybrid Static Single Assignment* language, as presented int [Connecting Reversible and Classical Computing Through Hybrid SSA](https://www.springerprofessional.de/en/connecting-reversible-and-classical-computing-through-hybrid-ssa/27143942).
It does not have a polished interface yet and includes some further development compared to the paper, so please get into touch with any questions.

## Contact

Lukas Gail - *lukas.gail@mni.thm.de*